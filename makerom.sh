#!/bin/bash

if [ ! -e ./system ]
then
  echo "Can't find 'system' for ROM"
  exit 1
fi

if [ ! -e ./META-INF ]
then
  echo "Can't find 'META-INF' for ROM"
  exit 1
fi

# Backup the build.prop file
cp system/build.prop system/build.prop.orig

# Main execution
main()
{
  echo "Deleting directories & files in system/"
  remove system/lost+found
  remove system/container
  remove system/preload
  remove system/preloadFotaOnly
  remove system/tts
  remove system/voicebargeindata
  remove system/hidden
  remove system/product
  
  remove_apps
  remove_bin
  remove_else
  remove_permissions
  remove_priv-app
  remove_knox
  remove_framework
  remove_etc
  remove_various
  copy_apps
  copy_files
  build_prop
  patching
}

#============================================================================
# Purge samsung bloatware from system/app
#============================================================================
function remove_apps() {

  echo "Removing bloatware in system/app"
  remove system/app/AASAservice
  remove system/app/AdvSoundDetector2015
  remove system/app/AllshareFileShare
  remove system/app/AllshareMediaShare
  remove system/app/AntHalService
  remove system/app/ANTPlusPlugins
  remove system/app/ANTPlusTest
  remove system/app/ApexService
  remove system/app/AppLinker
  remove system/app/ARCore
  remove system/app/AutomationTest_FB
  remove system/app/BasicDreams
  remove system/app/BBCAgent
  remove system/app/BCService
  remove system/app/BluetoothMidiService
  remove system/app/BluetoothTest
  remove system/app/BookmarkProvider
  remove system/app/bootagent
  remove system/app/CarrierDefaultApp
  remove system/app/ChocoEUKor
  remove system/app/Chrome
  remove system/app/ChromeCustomizations
  remove system/app/ClipboardEdge
  remove system/app/ClipboardSaveService
  remove system/app/ClipboardUIService
  remove system/app/ClockPackage
  remove system/app/CnnPanel
  remove system/app/CocktailQuickTool
  remove system/app/CompanionDeviceManager
  remove system/app/CoolEUKor
  remove system/app/CtsShimPrebuilt
  remove system/app/DAAgent
  remove system/app/DictDiotekForSec
  remove system/app/DRParser
  remove system/app/DsmsAPK
  remove system/app/EasterEgg
  remove system/app/EasymodeContactsWidget81
  remove system/app/EasyOneHand3
  remove system/app/EdmSimPinService
  remove system/app/EmergencyLauncher
  remove system/app/EmergencyModeService
  remove system/app/EmergencyProvider
  remove system/app/ESEServiceAgent
  remove system/app/Facebook_stub
  remove system/app/FactoryCameraFB
  remove system/app/FBAppManager_NS
  remove system/app/FlipboardBriefing
  remove system/app/Foundation
  remove system/app/GameOptimizer
  remove system/app/GameOptimizingService
  remove system/app/GearManagerStub
  remove system/app/Gmail2
  remove system/app/GooglePrintRecommendationService
  remove system/app/GoogleTTS
  remove system/app/GoogleVrServices
  remove system/app/HandwritingService
  remove system/app/HiyaService
  remove system/app/ImsSettings
  remove system/app/InteractivePanoramaViewer_WQHD
  remove system/app/KidsHome_Installer
  remove system/app/KnoxAttestationAgent
  remove system/app/LinkSharing_v*
  remove system/app/Maps
  remove system/app/MDMApp
  remove system/app/MirrorLink
  remove system/app/mldapchecker
  remove system/app/MotionPanoramaViewer
  remove system/app/MSSkype_stub
  remove system/app/Omc
  remove system/app/PacProcessor
  remove system/app/PartnerBookmarksProvider
  remove system/app/Personalization
  remove system/app/PhotoTable
  remove system/app/PlayAutoInstallConfig
  remove system/app/RcsSettings
  remove system/app/RootPA
  remove system/app/RoseEUKor
  remove system/app/SafetyInformation
  remove system/app/SamsungCalendar
  remove system/app/SamsungConnect_Stub
  remove system/app/SamsungIMEv*
  remove system/app/SamsungPassAutofill_v*
  remove system/app/SamsungTTS
  remove system/app/SBrowserEdge
  remove system/app/SCPMClient_N
  remove system/app/SecFactoryPhoneTest
  remove system/app/SecHTMLViewer
  remove system/app/SecureElement
  remove system/app/SecurityLogAgent
  remove system/app/SecurityProviderSEC
  remove system/app/SelfMotionPanoramaViewer
  remove system/app/ShortcutBackupService
  remove system/app/SilentLog
  remove system/app/SimMobilityKit
  remove system/app/SLocation
  remove system/app/SlowMotionVideoEditor
  remove system/app/SmartCapture
  remove system/app/SmartFittingService
  remove system/app/SmartMirroring
  remove system/app/SmartReminder
  remove system/app/SmartSwitchAgent
  remove system/app/SMusicPicker
  remove system/app/SPdfNote
  remove system/app/SplitSoundService
  remove system/app/STalkback
  remove system/app/Stk
  remove system/app/Stk2
  remove system/app/StoryEditor_Dream_N
  remove system/app/sveservice
  remove system/app/TetheringAutomation
  remove system/app/Traceur
  remove system/app/UniversalMDMClient
  remove system/app/UniversalSwitch
  remove system/app/USBSettings
  remove system/app/VideoEditorLite_Dream_N
  remove system/app/VideoTrimmer
  remove system/app/VisionIntelligence*
  remove system/app/vsimservice
  remove system/app/WallpaperBackup
  remove system/app/Weather_*
  remove system/app/WebManual
  remove system/app/WifiGuider
  remove system/app/WlanTest
  remove system/app/YahooEdgeFinance
  remove system/app/YahooEdgeSports
  remove system/app/YouTube
}

#============================================================================
# Cleanup binaries duplicated by busybox from system/bin & system/vendor/bin
#============================================================================
function remove_bin() {
  echo "Removing duplicated binaries in system/bin"
  remove system/bin/acpi
  remove system/bin/app_process
  remove system/bin/base64
  remove system/bin/basename
  remove system/bin/blockdev
  remove system/bin/bunzip2
  remove system/bin/bzcat
  remove system/bin/cal
  remove system/bin/cat
  remove system/bin/chcon
  remove system/bin/chgrp
  remove system/bin/chmod
  remove system/bin/chown
  remove system/bin/chroot
  remove system/bin/chrt
  remove system/bin/cksum
  remove system/bin/clear
  remove system/bin/cmp
  remove system/bin/comm
  remove system/bin/cp
  remove system/bin/cpio
  remove system/bin/cut
  remove system/bin/dalvikvm
  remove system/bin/date
  remove system/bin/dd
  remove system/bin/df
  remove system/bin/diff
  remove system/bin/dirname
  remove system/bin/dmesg
  remove system/bin/dos2unix
  remove system/bin/du
  remove system/bin/echo
  remove system/bin/egrep
  remove system/bin/env
  remove system/bin/expand
  remove system/bin/expr
  remove system/bin/fallocate
  remove system/bin/false
  remove system/bin/fgrep
  remove system/bin/file
  remove system/bin/find
  remove system/bin/flock
  remove system/bin/fmt
  remove system/bin/free
  remove system/bin/getenforce
  remove system/bin/getevent
  remove system/bin/getprop
  remove system/bin/groups
  remove system/bin/gunzip
  remove system/bin/gzip
  remove system/bin/head
  remove system/bin/hostname
  remove system/bin/hwclock
  remove system/bin/id
  remove system/bin/ifconfig
  remove system/bin/inotifyd
  remove system/bin/insmod
  remove system/bin/install-recovery.sh
  remove system/bin/ionice
  remove system/bin/iorenice
  remove system/bin/ip-wrapper-1.0
  remove system/bin/ip6tables-restore
  remove system/bin/ip6tables-save
  remove system/bin/ip6tables-wrapper-1.0
  remove system/bin/iptables-restore
  remove system/bin/iptables-save
  remove system/bin/iptables-wrapper-1.0
  remove system/bin/kill
  remove system/bin/killall
  remove system/bin/linker_asan
  remove system/bin/linker_asan64
  remove system/bin/ln
  remove system/bin/load_policy
  remove system/bin/log
  remove system/bin/logname
  remove system/bin/losetup
  remove system/bin/ls
  remove system/bin/lsmod
  remove system/bin/lsof
  remove system/bin/lspci
  remove system/bin/lsusb
  remove system/bin/md5sum
  remove system/bin/microcom
  remove system/bin/mkdir
  remove system/bin/mkfifo
  remove system/bin/mkfs.ext2
  remove system/bin/mkfs.ext3
  remove system/bin/mkfs.ext4
  remove system/bin/mknod
  remove system/bin/mkswap
  remove system/bin/mktemp
  remove system/bin/modinfo
  remove system/bin/modprobe
  remove system/bin/more
  remove system/bin/mount
  remove system/bin/mountpoint
  remove system/bin/mv
  remove system/bin/ndc-wrapper-1.0
  remove system/bin/netstat
  remove system/bin/newfs_msdos
  remove system/bin/nice
  remove system/bin/nl
  remove system/bin/nohup
  remove system/bin/od
  remove system/bin/paste
  remove system/bin/patch
  remove system/bin/pgrep
  remove system/bin/pidof
  remove system/bin/pkill
  remove system/bin/pmap
  remove system/bin/printenv
  remove system/bin/printf
  remove system/bin/ps
  remove system/bin/pwd
  remove system/bin/readlink
  remove system/bin/realpath
  remove system/bin/renice
  remove system/bin/restorecon
  remove system/bin/rm
  remove system/bin/rmdir
  remove system/bin/rmmod
  remove system/bin/runcon
  remove system/bin/sed
  remove system/bin/sendevent
  remove system/bin/seq
  remove system/bin/setenforce
  remove system/bin/setprop
  remove system/bin/setsid
  remove system/bin/sha1sum
  remove system/bin/sha224sum
  remove system/bin/sha256sum
  remove system/bin/sha384sum
  remove system/bin/sha512sum
  remove system/bin/sleep
  remove system/bin/sort
  remove system/bin/split
  remove system/bin/start
  remove system/bin/stat
  remove system/bin/stop
  remove system/bin/strings
  remove system/bin/stty
  remove system/bin/swapoff
  remove system/bin/swapon
  remove system/bin/sync
  remove system/bin/sysctl
  remove system/bin/tac
  remove system/bin/tail
  remove system/bin/tar
  remove system/bin/taskset
  remove system/bin/tc-wrapper-1.0
  remove system/bin/tee
  remove system/bin/time
  remove system/bin/timeout
  remove system/bin/top
  remove system/bin/touch
  remove system/bin/tr
  remove system/bin/true
  remove system/bin/truncate
  remove system/bin/tty
  remove system/bin/ulimit
  remove system/bin/umount
  remove system/bin/uname
  remove system/bin/uniq
  remove system/bin/unix2dos
  remove system/bin/uptime
  remove system/bin/usleep
  remove system/bin/uudecode
  remove system/bin/uuencode
  remove system/bin/vmstat
  remove system/bin/wc
  remove system/bin/which
  remove system/bin/whoami
  remove system/bin/xargs
  remove system/bin/xxd
  remove system/bin/yes
  remove system/bin/zcat

  echo "Removing duplicated binaries in system/vendor/bin"
  remove system/vendor/bin/acpi
  remove system/vendor/bin/base64
  remove system/vendor/bin/basename
  remove system/vendor/bin/blockdev
  remove system/vendor/bin/cal
  remove system/vendor/bin/cat
  remove system/vendor/bin/chcon
  remove system/vendor/bin/chgrp
  remove system/vendor/bin/chmod
  remove system/vendor/bin/chown
  remove system/vendor/bin/chroot
  remove system/vendor/bin/chrt
  remove system/vendor/bin/cksum
  remove system/vendor/bin/clear
  remove system/vendor/bin/cmp
  remove system/vendor/bin/comm
  remove system/vendor/bin/cp
  remove system/vendor/bin/cpio
  remove system/vendor/bin/cut
  remove system/vendor/bin/date
  remove system/vendor/bin/dd
  remove system/vendor/bin/df
  remove system/vendor/bin/diff
  remove system/vendor/bin/dirname
  remove system/vendor/bin/dmesg
  remove system/vendor/bin/dos2unix
  remove system/vendor/bin/du
  remove system/vendor/bin/echo
  remove system/vendor/bin/egrep
  remove system/vendor/bin/env
  remove system/vendor/bin/expand
  remove system/vendor/bin/expr
  remove system/vendor/bin/fallocate
  remove system/vendor/bin/false
  remove system/vendor/bin/fgrep
  remove system/vendor/bin/file
  remove system/vendor/bin/find
  remove system/vendor/bin/flock
  remove system/vendor/bin/fmt
  remove system/vendor/bin/free
  remove system/vendor/bin/getenforce
  remove system/vendor/bin/getevent
  remove system/vendor/bin/getprop
  remove system/vendor/bin/groups
  remove system/vendor/bin/gunzip
  remove system/vendor/bin/gzip
  remove system/vendor/bin/head
  remove system/vendor/bin/hostname
  remove system/vendor/bin/hwclock
  remove system/vendor/bin/id
  remove system/vendor/bin/ifconfig
  remove system/vendor/bin/inotifyd
  remove system/vendor/bin/insmod
  remove system/vendor/bin/ionice
  remove system/vendor/bin/iorenice
  remove system/vendor/bin/kill
  remove system/vendor/bin/killall
  remove system/vendor/bin/ln
  remove system/vendor/bin/load_policy
  remove system/vendor/bin/log
  remove system/vendor/bin/logname
  remove system/vendor/bin/losetup
  remove system/vendor/bin/ls
  remove system/vendor/bin/lsmod
  remove system/vendor/bin/lsof
  remove system/vendor/bin/lspci
  remove system/vendor/bin/lsusb
  remove system/vendor/bin/md5sum
  remove system/vendor/bin/microcom
  remove system/vendor/bin/mkdir
  remove system/vendor/bin/mkfifo
  remove system/vendor/bin/mknod
  remove system/vendor/bin/mkswap
  remove system/vendor/bin/mktemp
  remove system/vendor/bin/modinfo
  remove system/vendor/bin/modprobe
  remove system/vendor/bin/more
  remove system/vendor/bin/mount
  remove system/vendor/bin/mountpoint
  remove system/vendor/bin/mv
  remove system/vendor/bin/netstat
  remove system/vendor/bin/newfs_msdos
  remove system/vendor/bin/nice
  remove system/vendor/bin/nl
  remove system/vendor/bin/nohup
  remove system/vendor/bin/od
  remove system/vendor/bin/paste
  remove system/vendor/bin/patch
  remove system/vendor/bin/pgrep
  remove system/vendor/bin/pidof
  remove system/vendor/bin/pkill
  remove system/vendor/bin/pmap
  remove system/vendor/bin/printenv
  remove system/vendor/bin/printf
  remove system/vendor/bin/ps
  remove system/vendor/bin/pwd
  remove system/vendor/bin/readlink
  remove system/vendor/bin/realpath
  remove system/vendor/bin/renice
  remove system/vendor/bin/restorecon
  remove system/vendor/bin/rm
  remove system/vendor/bin/rmdir
  remove system/vendor/bin/rmmod
  remove system/vendor/bin/runcon
  remove system/vendor/bin/sed
  remove system/vendor/bin/sendevent
  remove system/vendor/bin/seq
  remove system/vendor/bin/setenforce
  remove system/vendor/bin/setprop
  remove system/vendor/bin/setsid
  remove system/vendor/bin/sha1sum
  remove system/vendor/bin/sha224sum
  remove system/vendor/bin/sha256sum
  remove system/vendor/bin/sha384sum
  remove system/vendor/bin/sha512sum
  remove system/vendor/bin/sleep
  remove system/vendor/bin/sort
  remove system/vendor/bin/split
  remove system/vendor/bin/start
  remove system/vendor/bin/stat
  remove system/vendor/bin/stop
  remove system/vendor/bin/strings
  remove system/vendor/bin/stty
  remove system/vendor/bin/swapoff
  remove system/vendor/bin/swapon
  remove system/vendor/bin/sync
  remove system/vendor/bin/sysctl
  remove system/vendor/bin/tac
  remove system/vendor/bin/tail
  remove system/vendor/bin/tar
  remove system/vendor/bin/taskset
  remove system/vendor/bin/tee
  remove system/vendor/bin/time
  remove system/vendor/bin/timeout
  remove system/vendor/bin/top
  remove system/vendor/bin/touch
  remove system/vendor/bin/tr
  remove system/vendor/bin/true
  remove system/vendor/bin/truncate
  remove system/vendor/bin/tty
  remove system/vendor/bin/ulimit
  remove system/vendor/bin/umount
  remove system/vendor/bin/uname
  remove system/vendor/bin/uniq
  remove system/vendor/bin/unix2dos
  remove system/vendor/bin/uptime
  remove system/vendor/bin/usleep
  remove system/vendor/bin/uudecode
  remove system/vendor/bin/uuencode
  remove system/vendor/bin/vmstat
  remove system/vendor/bin/wc
  remove system/vendor/bin/which
  remove system/vendor/bin/whoami
  remove system/vendor/bin/xargs
  remove system/vendor/bin/xxd
  remove system/vendor/bin/yes
  remove system/vendor/bin/zcat
}

#============================================================================
# Remove multiple files across directories
#============================================================================
function remove_else() {
  echo "Removing more bloatware.."
  remove system/cameradata/kidsmode_feature.cl.bin
  
  remove system/etc/secure_storage/com.samsung.android.fast
  remove system/etc/secure_storage/com.samsung.android.fast!dex
  remove system/etc/secure_storage/com.samsung.android.personalpage*
  remove system/etc/secure_storage/com.samsung.android.providers*
  remove system/etc/secure_storage/com.samsung.android.securitylogagent*
  remove system/etc/secure_storage/com.samsung.android.sm.devicesecurity*
  
  remove system/etc/sysconfig/bixbyagent.xml
  remove system/etc/sysconfig/clockpackageapp.xml
  remove system/etc/sysconfig/devicesecurityapp.xml
  remove system/etc/sysconfig/easyonehandapp.xml
  remove system/etc/sysconfig/facebook-hiddenapi-package-whitelist.xml
  remove system/etc/sysconfig/galaxyapps.xml
  remove system/etc/sysconfig/google_vr_build.xml
  remove system/etc/sysconfig/reminderapp.xml
  remove system/etc/sysconfig/samsungaccount.xml
  remove system/etc/sysconfig/samsungauthframework.xml
  remove system/etc/sysconfig/samsungexperienceservice.xml
  remove system/etc/sysconfig/samsungpassapp.xml
  remove system/etc/sysconfig/samsungpushservice.xml
  remove system/etc/sysconfig/samsungunifiedwfcapp.xml #(mabelisle) added 19-03-2019

  remove system/etc/theme
  
  remove system/etc/public.libraries-email.samsung.txt
  remove system/etc/public.libraries-knox.samsung.txt
  remove system/etc/public.libraries-secsmartcard.samsung.txt
  remove system/etc/public.libraries-spensdk.samsung.txt
  remove system/etc/public.libraries-videoeditor.samsung.txt
  remove system/etc/removable_preload.txt
  
  remove system/recovery-from-boot.p
  remove system/tima_measurement_info
  remove system/info.extra
  remove system/lkm_sec_info
}

#============================================================================
# Removing permissions from system/etc/permissions
#============================================================================
function remove_permissions() {
  echo "Removing permissions"
  remove system/etc/permissions/allshare_library.xml
  remove system/etc/permissions/android.software.live_wallpaper.xml
  remove system/etc/permissions/android.software.verified_boot.xml
  remove system/etc/permissions/authfw.xml
  remove system/etc/permissions/com.dsi.ant.antradio_library.xml
  remove system/etc/permissions/com.samsung.bbc.xml
  remove system/etc/permissions/com.samsung.feature.galaxyfinder_v7.xml
  remove system/etc/permissions/com.samsung.feature.mirrorlink_fw_level8.xml
  remove system/etc/permissions/com.samsung.feature.samsung_experience_mobile.xml
  remove system/etc/permissions/com.sec.android.secimaging.faceAR.xml
  remove system/etc/permissions/com.sec.android.smartface.smart_stay.xml
  remove system/etc/permissions/com.sec.feature.barcode_emulator.xml
  remove system/etc/permissions/com.sec.feature.cocktailpanel.xml
  remove system/etc/permissions/com.sec.feature.cover.clearcover.xml
  remove system/etc/permissions/com.sec.feature.cover.flip.xml
  remove system/etc/permissions/com.sec.feature.cover.nfcledcover.xml
  remove system/etc/permissions/com.sec.feature.cover.sview.xml
  remove system/etc/permissions/com.sec.feature.cover.xml
  remove system/etc/permissions/com.sec.feature.edge_v03.xml
  remove system/etc/permissions/com.sec.feature.findo.xml
  #remove system/etc/permissions/com.sec.feature.nfc_authentication.xml
  #remove system/etc/permissions/com.sec.feature.nfc_authentication_cover.xml
  remove system/etc/permissions/com.sec.feature.people_edge_notification.xml
  remove system/etc/permissions/com.sec.feature.slocation_level3.xml
  remove system/etc/permissions/com.sec.smartcard.auth.xml
  remove system/etc/permissions/knoxanalytics.xml
  remove system/etc/permissions/knoxsdk_edm.xml
  remove system/etc/permissions/knoxsdk_mdm.xml
  remove system/etc/permissions/privapp-permissions-com.android.wallpaper.livepicker.xml 
  remove system/etc/permissions/privapp-permissions-com.knox.vpn.proxyhandler.xml
  remove system/etc/permissions/privapp-permissions-com.osp.app.signin.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.accessibility.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.app.appsedge.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.app.cocktailbarservice.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.app.galaxyfinder.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.app.ledcoverdream.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.app.omcagent.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.app.settings.bixby.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.app.spage.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.app.taskedge.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.app.telephonyui.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.authfw.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.beaconmanager.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.bixby.agent.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.bixby.service.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.bixby.wakeup.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.contacts.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.dialer.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.dqagent.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.easysetup.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.email.provider.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.fmm.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.game.gamehome.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.game.gametools.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.hmt.vrsvc.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.incallui.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.kgclient.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.knox.analytics.uploader.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.knox.containeragent.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.knox.containercore.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.knox.containerdesktop.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.lool.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.mateagent.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.messaging.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.mobileservice.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.networkdiagnostic.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.providers.context.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.rubin.app.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.samsungpass.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.samsungpositioning.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.samsungsocial.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.scloud.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.service.peoplestripe.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.setting.multisound.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.sm.devicesecurity_v6.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.smartface.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.spayfw.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.spdclient.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.svcagent.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.themecenter.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.android.timezone.updater.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.cmh.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.crane.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.desktopsystemui.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.klmsagent.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.knox.securefolder.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.sec.android.application.csc.xml
  remove system/etc/permissions/privapp-permissions-com.samsung.systemui.bixby2.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.app.billing.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.app.desktoplauncher.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.app.factorykeystring.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.app.hwmoduletest.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.app.launcher.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.app.magnifier.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.app.myfiles.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.app.popupcalculator.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.app.ringtoneBR.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.app.safetyassurance.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.app.samsungapps.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.app.SecSetupWizard.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.app.servicemodeapp.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.app.shealth.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.app.soundalive.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.AutoPreconfig.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.cover.ledcover.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.daemonapp.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.diagmonagent.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.gallery3d.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.mimage.gear360editor.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.mimage.photoretouching.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.ofviewer.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.omc.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.preloadinstaller.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.provider.badge.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.RilServiceModeApp.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.soagent.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.uibcvirtualsoftkey.xml
  remove system/etc/permissions/privapp-permissions-com.sec.android.widgetapp.samsungapps.xml
  remove system/etc/permissions/privapp-permissions-com.sec.app.RilErrorNotifier.xml
  remove system/etc/permissions/privapp-permissions-com.sec.epdg.xml
  remove system/etc/permissions/privapp-permissions-com.sec.epdgtestapp.xml
  remove system/etc/permissions/privapp-permissions-com.sec.factory.xml
  remove system/etc/permissions/privapp-permissions-com.sec.hearingadjust-v7P.xml
  remove system/etc/permissions/privapp-permissions-com.sec.ims.xml
  remove system/etc/permissions/privapp-permissions-com.sec.imslogger.xml
  remove system/etc/permissions/privapp-permissions-com.sec.imsservice.xml
  remove system/etc/permissions/privapp-permissions-com.sec.location.nsflp2_v3_5.xml
  remove system/etc/permissions/privapp-permissions-com.sec.spp.push.xml
  remove system/etc/permissions/privapp-permissions-com.skms.android.agent.xml
  remove system/etc/permissions/privapp-permissions-com.wsomacp.xml
  remove system/etc/permissions/privapp-permissions-com.wssyncmldm.xml
  remove system/etc/permissions/privapp-permissions-de.axelspringer.yana.zeropage.xml
  remove system/etc/permissions/privapp-permissions-facebook.xml
  remove system/etc/permissions/privapp-permissions-themestore.xml
  remove system/etc/permissions/sec_mdm.xml
  remove system/etc/permissions/shealth_sw_pedometer_paused.xml
  remove system/etc/permissions/ve_sdk.xml
}
#============================================================================
# Removing permissions from system/etc/permissions
#============================================================================
function remove_priv-app() {
  echo "Removing bloatware in system/priv-app"
  #remove system/priv-app/ConfigUpdater
  #remove system/priv-app/ExternalStorageProvider
  #remove system/priv-app/InputDevices
  remove system/priv-app/ANTRadioService
  remove system/priv-app/AppsEdgePanel*
  remove system/priv-app/AssistantMenu_N
  remove system/priv-app/AuthFramework
  remove system/priv-app/AutoPreconfig
  remove system/priv-app/BadgeProvider_N
  remove system/priv-app/BeaconManager
  remove system/priv-app/BioFaceService
  remove system/priv-app/Bixby
  remove system/priv-app/BixbyAgentStub
  remove system/priv-app/BixbyHome
  remove system/priv-app/BixbyService
  remove system/priv-app/BixbyWakeup
  remove system/priv-app/CarrierConfig
  remove system/priv-app/CocktailBarService*
  remove system/priv-app/ContainerAgent*
  remove system/priv-app/ContextProvider
  remove system/priv-app/Crane
  remove system/priv-app/CSC
  remove system/priv-app/CtsShimPrivPrebuilt
  remove system/priv-app/DesktopModeUiService
  remove system/priv-app/DeviceKeystring
  remove system/priv-app/DeviceQualityAgent
  remove system/priv-app/DeviceTest
  remove system/priv-app/DexCommunity
  remove system/priv-app/DiagMonAgent
  remove system/priv-app/EasySetup
  remove system/priv-app/EmojiUpdater
  remove system/priv-app/EpdgService
  remove system/priv-app/Excel_SamsungStub
  remove system/priv-app/FBInstaller_NS
  remove system/priv-app/FBServices
  remove system/priv-app/Finder
  remove system/priv-app/Fmm
  remove system/priv-app/FotaAgent
  remove system/priv-app/GalaxyApps*
  remove system/priv-app/GalaxyAppsWidget_Phone_Dream
  remove system/priv-app/GameHome*
  remove system/priv-app/GameTools_Dream
  remove system/priv-app/Gear360*
  remove system/priv-app/GearVRService
  remove system/priv-app/GmsCore
  remove system/priv-app/GoogleDaydreamCustomization
  remove system/priv-app/GoogleFeedback
  remove system/priv-app/GooglePartnerSetup
  remove system/priv-app/Hearingdro_V*
  remove system/priv-app/Hotword*
  remove system/priv-app/HwModuleTest
  remove system/priv-app/ImsLogger*
  remove system/priv-app/imsservice
  remove system/priv-app/ImsTelephonyService
  remove system/priv-app/IPService
  remove system/priv-app/IrisUserTest
  remove system/priv-app/KeyguardWallpaperUpdator
  remove system/priv-app/KLMSAgent
  remove system/priv-app/knoxanalyticsagent
  remove system/priv-app/KnoxContainerDesktop
  remove system/priv-app/KnoxCore
  remove system/priv-app/KnoxDesktopLauncher
  remove system/priv-app/KnoxGuard
  remove system/priv-app/knoxvpnproxyhandler
  remove system/priv-app/LedCoverAppDream
  remove system/priv-app/LedCoverService
  remove system/priv-app/ManagedProvisioning
  remove system/priv-app/MateAgent
  remove system/priv-app/MediaLearning*
  remove system/priv-app/MtpDocumentsProvider
  remove system/priv-app/NetworkDiagnostic
  remove system/priv-app/NSDSWebApp
  remove system/priv-app/NSFusedLocation_v*
  remove system/priv-app/OmaCP
  remove system/priv-app/OMCAgent*
  remove system/priv-app/OneDrive_Samsung_v*
  remove system/priv-app/PaymentFramework
  remove system/priv-app/PeopleStripe
  remove system/priv-app/PhoneErrService
  remove system/priv-app/Phonesky
  remove system/priv-app/PhotoStudio_Beyond
  remove system/priv-app/PowerPoint_SamsungStub
  remove system/priv-app/PreloadInstaller
  remove system/priv-app/ProxyHandler
  remove system/priv-app/ringtoneBR
  remove system/priv-app/Rlc
  remove system/priv-app/Rubin*
  remove system/priv-app/SamsungAccount*
  remove system/priv-app/SamsungBilling
  remove system/priv-app/SamsungCalendarProvider
  remove system/priv-app/SamsungCamera
  remove system/priv-app/SamsungCloud*
  remove system/priv-app/SamsungContacts
  remove system/priv-app/SamsungDialer
  remove system/priv-app/SamsungExperienceService
  remove system/priv-app/SamsungGallery2018
  remove system/priv-app/SamsungInCallUI
  remove system/priv-app/SamsungMagnifier3
  remove system/priv-app/SamsungMessages_10.0
  remove system/priv-app/SamsungPass*
  remove system/priv-app/SamsungPositioning
  remove system/priv-app/SamsungSocial
  remove system/priv-app/SamsungVideoPlayer2016
  remove system/priv-app/SecLiveWallpapersPicker
  remove system/priv-app/SecMyFiles*
  remove system/priv-app/SecSetupWizard_Global
  remove system/priv-app/SecureFolder
  remove system/priv-app/SelectiveFocusViewer
  remove system/priv-app/SEMFactoryApp
  remove system/priv-app/SendHelpMessage
  remove system/priv-app/serviceModeApp_FB
  remove system/priv-app/SettingsBixby
  remove system/priv-app/SettingsReceiver
  remove system/priv-app/SKMSAgent
  remove system/priv-app/SmartCallProvider
  remove system/priv-app/SmartEpdgTestApp
  remove system/priv-app/smartfaceservice
  remove system/priv-app/SmartManager_v*
  remove system/priv-app/SmartSwitchAssistant
  remove system/priv-app/SOAgent
  remove system/priv-app/SoundAlive_*
  remove system/priv-app/SPDClient
  remove system/priv-app/SPPPushClient*
  remove system/priv-app/StatementService
  remove system/priv-app/StickerCenter
  remove system/priv-app/StickerFaceAR
  remove system/priv-app/StickerProvider
  remove system/priv-app/StickerStamp
  #remove system/priv-app/StorageManager
  remove system/priv-app/StoryService
  remove system/priv-app/SuwScriptPlayer
  remove system/priv-app/SVCAgent
  remove system/priv-app/SVoicePLM
  remove system/priv-app/SystemUIBixby*
  remove system/priv-app/SystemUIDesktop
  remove system/priv-app/Tag
  remove system/priv-app/TaskEdgePanel*
  remove system/priv-app/TelephonyUI
  remove system/priv-app/ThemeCenter
  remove system/priv-app/TouchWizHome_*
  remove system/priv-app/Turbo
  remove system/priv-app/UIBCVirtualSoftkey
  remove system/priv-app/Upday
  remove system/priv-app/Velvet
  remove system/priv-app/Vision*
  remove system/priv-app/WallpaperCropper
  remove system/priv-app/WallpaperCropper2
  remove system/priv-app/wallpaper-res
  remove system/priv-app/Word_SamsungStub
}

#============================================================================
# Remove knox from various xml files
#============================================================================
function remove_knox() {
  echo "Remove knox from various xml files"
  grep -v knox system/etc/permissions/platform.xml > /tmp/platform.xml
  cp /tmp/platform.xml system/etc/permissions/
  grep -v knox system/etc/permissions/privapp-permissions-platform.xml > /tmp/privapp-permissions-platform.xml
  cp /tmp/privapp-permissions-platform.xml system/etc/permissions/
  grep -v knox system/vendor/etc/public.libraries.txt > /tmp/public.libraries.txt
  cp /tmp/public.libraries.txt system/vendor/etc/public.libraries.txt
  grep -v 'knox\.' system/vendor/etc/selinux/apm/authorize.xml | grep -v knoxcts > /tmp/authorize.xml
  sed -e 's/knox_/xonk_/g' /tmp/authorize.xml > system/vendor/etc/selinux/apm/authorize.xml
}

function remove_framework() {

  echo "Deleting files in system/framework/"
  remove system/framework/com.dsi.ant.antradio_library.jar
  remove system/framework/com.samsung.bbccommon.jar
  remove system/framework/gamemanager.jar
  remove system/framework/gamesdk.jar
  remove system/framework/gamesdkWrapper.jar
  remove system/framework/oat/arm/allshare.* #Samsung AllShare, also called Samsung Link, is a service that lets Samsung devices, Samsung TVs and your PC access and share photos
  remove system/framework/oat/arm/com.dsi.ant.antradio_library.*
  remove system/framework/oat/arm/com.samsung.bbccommon.*
  remove system/framework/oat/arm/gamesdk.*
  remove system/framework/oat/arm/gamesdkWrapper.*
  remove system/framework/oat/arm/secsmartcard.*
  remove system/framework/oat/arm/videoeditor_sdk.*
  remove system/framework/oat/arm64/allshare.* #Samsung AllShare, also called Samsung Link, is a service that lets Samsung devices, Samsung TVs and your PC access and share photos
  remove system/framework/oat/arm64/com.dsi.ant.antradio_library.*
  remove system/framework/oat/arm64/com.samsung.bbccommon.*
  remove system/framework/oat/arm64/gamesdk.*
  remove system/framework/oat/arm64/gamemanager.*
  remove system/framework/oat/arm64/secsmartcard.*
  remove system/framework/oat/arm64/videoeditor_sdk.*
  remove system/framework/secsmartcard.jar
  remove system/framework/videoeditor_sdk.jar
}

function remove_etc() {
  echo "Deleting directories & files in system/etc"
  #remove system/etc/apks_count_list.txt
  #remove system/etc/ASKSTS.xml
  #remove system/etc/DefaultGrantPermission.xml
  #remove system/etc/default-permissions/*
  #remove system/etc/epdg_apns_conf.xml
  #remove system/etc/feature_default.xml
  #remove system/etc/fota.cer
  remove system/etc/fs_config_dirs
  remove system/etc/fs_config_files
  #remove system/etc/irremovable_list.txt
  #remove system/etc/multisound_*
  #remove system/etc/nfc_key
  #remove system/etc/nfcee_access.xml
  #remove system/etc/NOTICE.html.gz
  #remove system/etc/nwk_info.xml
  #remove system/etc/old-apns-conf.xml
  #remove system/etc/PAICheck.xml
  #remove system/etc/purenandpackages.txt
  #remove system/etc/purenandpackages_pref.txt
  #remove system/etc/recovery-resource.dat
  #remove system/etc/restart_radio_process.sh
  #remove system/etc/ringtones_count_list.txt
  #remove system/etc/security/oculus.crt
  #remove system/etc/security_profile.dat # evolved packet data gateway (network security)
  #remove system/etc/userdata_apks_count_list.txt*
  #remove system/etc/vpl_apks_count_list.txt*
  #remove system/etc/xtables.lock
}

function remove_various() {

  echo "Deleting files in system/etc/preferred-apps"
  remove system/etc/preferred-apps/com.samsung.android.email.provider.xml
  remove system/etc/preferred-apps/com.sec.android.gallery3d.xml
  
  echo "Deleting files from system/etc/init"
  remove system/etc/init/powersnd.rc
  remove system/etc/init/uncrypt.rc
  remove system/etc/init/vdc.rc
  remove system/etc/init/bootstat.rc
  remove system/etc/init/wifi-events.rc
  remove system/etc/init/mdnsd.rc
  
  echo "Deleting files from system/cameradata"
  remove system/cameradata/ThemeShot/MaskInfo_v3_download.conf
  
  echo "Removing VR"
  remove system/bin/vr
  remove system/etc/permissions/android.hardware.vr.high_performance.xml
  remove system/framework/oat/arm/vr.odex
  remove system/framework/oat/arm/vr.vdex
  remove system/framework/oat/arm64/vr.odex
  remove system/framework/oat/arm64/vr.vdex
  remove system/framework/vr.jar
  remove system/vendor/bin/hw/android.hardware.vr@1.0-service
  remove system/vendor/etc/init/android.hardware.vr@1.0-service.rc
}

#============================================================================
# Copying apps
#============================================================================
function copy_apps() {
  echo "Copying apps into system/app"
  cp -R addons/app/* system/app
  echo "Copying apps into system/priv-app"
  cp -R addons/priv-app/* system/priv-app
}

#============================================================================
# Copying files
#============================================================================
function copy_files {
  echo "Copying files into system/etc"
  cp -R addons/etc/* system/etc
  
  echo "Copying files into system/framework"
  cp -R addons/framework/* system/framework
  
  echo "Installing boot animation"
  cp -R addons/media/* system/media
  
  echo "Installing Busybox"
  cp -R addons/xbin/* system/xbin
  
  echo "Moving compatibility_matrix"
  mv system/compatibility_matrix.xml system/vendor
  
  echo "Copyingfiles for camera"
  cp -R addons/cameradata/* system/cameradata
  
  echo "Creating init.d and scripts"
  mkdir -p system/etc/init.d/
  cp addons/init.d/LS00* system/etc/init.d/
  
  echo "Creating services.d and scripts"
  mkdir -p system/etc/services.d/
  cp addons/services.d/LS99* system/etc/services.d/
  
  echo "Adding script to /etc/init"
  cp addons/init/zzzfoobar66.rc system/etc/init/
}

#============================================================================
# Copying files
#============================================================================
function build_prop() {
  # patch build.prop
  echo "Patching build.prop (changing properties)"
  cp system/build.prop system/build.prop.orig
  grep -v 'ro.build.display.id' system/build.prop > /tmp/build.prop.tmp
  cp /tmp/build.prop.tmp system/build.prop
  # note: originally had set ro.config.knox=0 ... but then copy/paste did no longer work!
  # have to keep it set at ro.config.knox=v30 ...
  declare -a props=(
    's/ro.build.selinux=1/ro.build.selinux=0/g'
    's/persist.sys.storage_preload=1/persist.sys.storage_preload=0/g'
    's/ro.config.tima=1/ro.config.tima=0/g'
    's/ro.product.locale=en-GB/ro.product.locale=en_US/g'
    's/ro.config.dmverity=true/ro.config.dmverity=false/g'
    's/ro.config.knox=v30/ro.config.knox=v30/g'
    's/ro.config.timaversion=3.0/ro.config.timaversion=disabled/g'
    's/ro.config.ringtone=Over_the_Horizon.ogg/ro.config.ringtone=Beats.ogg/g'
    's/ro.config.notification_sound=Skyline.ogg/ro.config.notification_sound=Beep.ogg/g'
    's/ro.config.alarm_alert=Morning_Glory.ogg/ro.config.alarm_alert=Awaken.ogg/g' 
    's/ro.config.ringtone_2=Basic_Bell.ogg/ro.config.ringtone_2=Beats.ogg/g'
    's/ro.config.notification_sound_2=S_Charming_Bell.ogg/ro.config.notification_sound_2=Beep.ogg/g'
    's/ro.config.rm_preload_enabled=1/ro.config.rm_preload_enabled=0/g'
    's/ro.ril.gprsclass=10/ro.ril.gprsclass=12/g'
    's/ro.ril.hsxpa=1/ro.ril.hsxpa=3/g'
    's/ro.security.mdf.ux=Enabled/ro.security.mdf.ux=Disabled/g'
    's/ro.security.vaultkeeper.feature=1/ro.security.vaultkeeper.feature=0/g'
    's/net.dns1=8.8.8.8/net.dns1=1.1.1.1/g'
    's/net.dns2=8.8.4.4/net.dns2=1.0.0.1/g'
    's/ro.config.kap_default_on=true/ro.config.kap_default_on=false/g'
    's/ro.config.kap=true/ro.config.kap=false/g'
    's/ro.config.rkp=true/ro.config.rkp=false/g'
  )
  for i in ${props[@]}; do
    cat system/build.prop | sed $i > /tmp/build.prop.tmp
    cp /tmp/build.prop.tmp system/build.prop
  done
  echo "Patching build.prop (removing properties)"
  declare -a props=(
    ro.security.mdf.ver
    ro.security.wlan.ver
    ro.security.wlan.release
    ro.security.mdf.release
    ro.security.fips.ux
    ro.security.fips_bssl.ver
    ro.security.fips_skc.ver
    ro.security.fips_scrypto.ver
    ro.security.fips_fmp.ver
    security.ASKS.policy_version
    ro.com.google.clientidbase
    net.dns1
    net.dns2
  )
  #for i in ${props[@]}; do
  #  cat system/build.prop | grep -v $i > /tmp/build.prop.tmp
  #  cp /tmp/build.prop.tmp system/build.prop
  #done
  #echo "Patching build.prop (adding properties)"
  #cat /tmp/build.prop.tmp addons/extrabuild > system/build.prop
  #echo 'ro.csc.sales_code=PRO' >> system/build.prop
  echo "ro.build.display.id=GoogleWiz Pie built by mabelisle (`date +%Y-%m-%d`)" >> system/build.prop
}

#============================================================================
# Patching system/etc/sysconfig/framework-sysconfig.xml
#============================================================================

function patching() {
  echo "Patching system/etc/sysconfig/framework-sysconfig.xml"
  xmlstarlet fo system/etc/sysconfig/framework-sysconfig.xml | grep -v knox > /tmp/framework-sysconfig.xml
  cp /tmp/framework-sysconfig.xml system/etc/sysconfig
  grep -v 'samsung\.account' system/etc/sysconfig/framework-sysconfig.xml > /tmp/framework-sysconfig.xml
  cp /tmp/framework-sysconfig.xml system/etc/sysconfig
  grep -v 'kt\.wifiapi' system/etc/sysconfig/framework-sysconfig.xml > /tmp/framework-sysconfig.xml
  cp /tmp/framework-sysconfig.xml system/etc/sysconfig
  grep -v 'peoplestripe' system/etc/sysconfig/framework-sysconfig.xml > /tmp/framework-sysconfig.xml
  cp /tmp/framework-sysconfig.xml system/etc/sysconfig
  grep -v verizon system/etc/sysconfig/framework-sysconfig.xml > /tmp/framework-sysconfig.xml
  cp /tmp/framework-sysconfig.xml system/etc/sysconfig/framework-sysconfig.xml
}

#######################################################
# remove() Deleting directories & files with validation
# Globals:
#   None
# Arguments:
#   (String) File name, (String) Line number
# Returns:
#   None
#######################################################
function remove()
{
  if [ -e $1 ]; then
    rm -rf $1;
  else
    err "Error removing -> "\"$1\";
  fi
}

#######################################################
# err() Display error message with a timestamp
# Globals:
#   None
# Arguments:
#   (String) Error message
# Returns:
#   None
#######################################################
function err() {
  echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@";
}

#######################################################
# add_xml_header() Add an xml header
# Globals:
#   None
# Arguments:
#   (String) Filename, (String) Line number
# Returns:
#   None
#######################################################
function add_xml_header () {
	if [ -f "$1" ]
	then
    sed -i '1s/^/<?xml version="1.0" encoding="utf-8"?>/' $1
	else
		err "Error adding XML header on -> "\"$1\";
	fi
}

# Call main function
main "$@"; exit


#============================================================================
: << 'COMMENT'

#============================================================================

  echo "Deleting directories & files in system/usr"
  remove system/usr/srec
  remove system/usr/keylayout/Vendor_*

  #just keep en-gb and en-us
  tar cf /tmp/h.tar system/usr/hyphen-data/hyph-en-*
  remove system/usr/hyphen-data/*
  tar xf /tmp/h.tar
  remove /tmp/h.tar
  remove system/usr/hyphen-data/hyph-en-gb.lic.txt
  remove system/usr/hyphen-data/hyph-en-us.lic.txt

#============================================================================

  grep -v 'VERSION' system/etc/adp_rules.xml > /tmp/adp_rules.xml #Patch weird xmlstarlet error
  cp /tmp/adp_rules.xml system/etc

#============================================================================

  #echo "Deleting files in system/lib and system/lib64"
  #remove system/lib/slocation/ (mabelisle) not in pie 19-03-2019
  #remove system/lib64/slocation/ (mabelisle) not in pie 19-03-2019

#============================================================================

  echo "Deleting directories & files in system/vendor/etc/carrier"
  remove system/vendor/etc/carrier

#============================================================================

  #echo "Deleting *verizon* files in system (why the heck are they there in an EU model)"
  #find system -name '*verizon*' -exec rm -rf {} \;

#============================================================================

	echo "Patching files in system/etc"
	#these files have 2 root elements in XML which is not allowed
	grep -v 'VERSION value' system/etc/ASKSB.xml > /tmp/ASKSB.xml; cp /tmp/ASKSB.xml system/etc/ASKSB.xml
	grep -v 'VERSION value' system/etc/ASKSC.xml > /tmp/ASKSC.xml; cp /tmp/ASKSC.xml system/etc/ASKSC.xml
	grep -v 'VERSION value' system/etc/ASKSM.xml > /tmp/ASKSM.xml; cp /tmp/ASKSM.xml system/etc/ASKSM.xml
	grep -v 'VERSION value' system/etc/ASKSP.xml > /tmp/ASKSP.xml; cp /tmp/ASKSP.xml system/etc/ASKSP.xml
	grep -v 'VERSION value' system/etc/ASKSP.xml > /tmp/ASKSP.xml; cp /tmp/ASKSP.xml system/etc/ASKSTS.xml
	grep -v 'VERSION value' system/etc/ASKSW.xml > /tmp/ASKSW.xml; cp /tmp/ASKSW.xml system/etc/ASKSW.xml

#============================================================================

	echo "Adding XML headers in system/etc"
	add_xml_header system/etc/mixer_gains.xml
	#add_xml_header system/etc/mixer_gains_r00.xml (mabelisle) not in pie 19-03-2019
	add_xml_header system/etc/mixer_paths.xml
	#add_xml_header system/etc/mixer_paths_r00.xml (mabelisle) not in pie 19-03-2019
	#add_xml_header system/etc/mixer_paths_r01.xml (mabelisle) not in pie 19-03-2019
	add_xml_header system/etc/permissions/com.samsung.android.api.version.xml

#============================================================================

  echo "Patching system/etc/sysconfig/google.xml"
  # this will allow gms to be put into battery optimization mode
  grep -v '<allow-in-power-save package="com.google.android.gms" />'  system/etc/sysconfig/google.xml > /tmp/google.xml
  cp /tmp/google.xml system/etc/sysconfig/google.xml
	
#============================================================================

  echo "Deleting fonts"
  remove system/fonts/
  remove system/etc/fonts.xml

#============================================================================

  echo "Deleting and modifying files in system/media"
  remove system/media/audio/ringtones
  remove system/media/audio/notifications
  tar xf ~/GoogleWiz/additions/sounds/sounds.tar
  remove system/media/audio/ui/Media_preview_Over_the_horizon.ogg*
  #remove system/media/audio/ui/Dock.ogg* (mabelisle) not in pie 19-03-2019
  #remove system/media/audio/ui/Undock.ogg* (mabelisle) not in pie 19-03-2019
  remove system/media/audio/ui/WirelessChargingStarted.ogg*
  remove system/media/audio/ui/TW_*
  remove system/media/audio/ui/S_Beam_*
  remove system/media/audio/ui/Pen_*
  remove system/media/audio/ui/Bixby_BOS.ogg
  remove system/media/crypt_bootsamsung*
	
#============================================================================

  echo "Deleting binaries in system/bin and system/xbin"
  remove system/bin/samsungpowersoundplay
  remove system/bin/faced
  remove system/bin/irisd
  remove system/bin/bootchecker
  # remotedisplay is required for video playout
  # rm -rf system/bin/remotedisplay
  remove system/bin/edmaudit
  remove system/bin/at_distributor #(mabelisle) location changed 19-03-2019
  remove system/bin/auditd
  remove system/bin/diagexe
  #remove system/bin/apaservice (mabelisle) not in pie 19-03-2019
  #remove system/bin/jackservice (mabelisle) not in pie 19-03-2019
  remove system/bin/apexserver
  #remove system/bin/vaultkeeperd (mabelisle) not in pie 19-03-2019
  #remove system/bin/visiond  (mabelisle) not in pie 19-03-2019
  #remove system/xbin/jack_* (mabelisle) not in pie 19-03-2019
  remove system/bin/uiautomator
  remove system/bin/svc
  remove system/bin/resize2fs
  #rm -rf system/bin/bmgr
  remove system/bin/atrace
  remove system/xbin/cpustats
  remove system/bin/bootstat
  remove system/bin/tombstoned
  remove system/bin/mdf_fota
  remove system/bin/wlandutservice
  remove system/bin/tlc_server
  remove system/bin/ss_conn_daemon
  remove system/vendor/bin/argosd
  # next one is for a VPN server?
  remove system/bin/racoon
  # next is for wifi calling
  remove system/bin/imsd
  # the next one can also be disabled by ro.wsmd.enable=false
  # this is world shared memory for Samsung pay
  #remove system/bin/wsmd (mabelisle) not in pie 19-03-2019
  # not clear what these are used for, but if DR-daemon is stopped, logcat is flooded with following error:
  # SatsServiceData: Failed to connect daemon - java.io.IOException: Connection refused
  ##rm -rf system/vendor/bin/ddexe
  # according to Noxxxious smdex is used for adb
  # SMD-daemon ... disabling it does not seem to do any big harm
  remove system/bin/smdexe #(mabelisle) location changed 19-03-2019
  #remove system/bin/epmlogd (mabelisle) not in pie 19-03-2019
  remove system/bin/charon
  remove system/bin/srm.bin

#============================================================================


  
#============================================================================



  echo "Creating system/csc"
  mkdir system/csc/
  cp ~/GoogleWiz/additions/feature/customer.xml system/csc/
  
  echo "Setting system/csc/sales_code.dat to PRO"
  echo PRO > system/csc/sales_code.dat


#============================================================================

  echo "Copying permissions"
  cp ~/GoogleWiz/additions/gps/gps.conf system/etc/

#============================================================================

  echo "Copying executables"
  cp ~/GoogleWiz/additions/bin/* system/xbin

#============================================================================

  echo "Copying hosts file"
  cp ~/GoogleWiz/additions/hosts/hosts.short system/etc
  cp ~/GoogleWiz/additions/hosts/hosts.long system/etc
  cp ~/GoogleWiz/additions/hosts/hosts.medium system/etc

#============================================================================

  echo "Copying libraries"
  cp ~/GoogleWiz/additions/lib/libblurdetection.so system/lib

#============================================================================

  # this tricks the device into being a Pixel but has impact on photos app
  tar xf ~/GoogleWiz/additions/pixel/PixelEtc.tar
  cp ~/GoogleWiz/additions/pixel/pixel_2018_exclusive.xml system/etc/sysconfig/
  cp ~/GoogleWiz/additions/pixel/google-hiddenapi-package-whitelist.xml system/etc/sysconfig/
  cp ~/GoogleWiz/additions/pixel/hiddenapi-package-whitelist.xml system/etc/sysconfig/

#============================================================================

  cp ~/GoogleWiz/additions/apns-conf.xml system/etc/
  cp ~/GoogleWiz/additions/feature/feature.xml system/csc/
  cp ~/GoogleWiz/additions/feature/floating_feature.xml system/etc/floating_feature.xml
  cp ~/GoogleWiz/additions/media/media_profiles.xml system/etc/
  # overlays
  tar xf ~/GoogleWiz/additions/overlays/overlays.tar
  # no round icons for GoogleWiz variants
  remove system/vendor/overlay/RoundIconsMaskOverlay.apk

#============================================================================

  chmod +x system/xbin/*

#============================================================================

  mkdir -p system/etc/bash
  cp ~/GoogleWiz/additions/bash/bashrc system/etc/bash/
  cp ~/GoogleWiz/additions/bash/bash_aliases system/etc/bash/
  cp ~/GoogleWiz/additions/bash/mkshrc system/etc

#============================================================================

  echo "Installing android P fonts"
  tar xf ~/GoogleWiz/additions/fonts/AndroidPFontsLarge.tar 2> /dev/null
  tar xf ~/GoogleWiz/additions/fonts/AndroidPFonts.tar 2> /dev/null
  tar xf ~/GoogleWiz/additions/fonts/AODfonts.tar 2> /dev/null
  cp ~/GoogleWiz/additions/fonts/fonts.xml system/etc/

#============================================================================

  # generate symlinks for updated file
  echo "Generating SYMLINKS file"
  find system -type l -exec ls -Foh {} \; | sort | awk '{print "symlink(\"" $10 "\", \"" $8 "\");"}' | sed 's/\*//g' | sed 's/"system/"\/system/g' > SYMLINKS
  # remove symlinks
  echo "Removing symlinks in system"
  find system -type l -exec rm {} \;

#============================================================================

  echo "Adding aptx bluetooth streaming"
  tar xf ~/GoogleWiz/additions/aptx/aptx.tar -C system

#============================================================================

  # rename all .apk files as 'base.apk'
  echo "Renaming apk files in system/app"
  APKF=`find system/app -name '*.apk' | grep -v 'base.apk' | grep -v 'split_config'`
  for APK in $APKF ; do
    BN=`basename $APK`
    SN=`echo $BN | sed -e 's/\.apk//g'`
    DN=`dirname $APK`
    if [ ! -d $DN/oat ] ; then
      mv $APK $DN/base.apk
    fi
  done
  echo "Renaming apk files in system/priv-app"
  APKF=`find system/priv-app -name '*.apk' | grep -v 'base.apk' | grep -v 'split_config' `
  for APK in $APKF ; do
    BN=`basename $APK`
    SN=`echo $BN | sed -e 's/\.apk//g'`
    DN=`dirname $APK`
    if [ ! -d $DN/oat ] ; then
      mv $APK $DN/base.apk
    fi
  done

#============================================================================

  echo "Optimizing XML files in system"
  # remove comments from XMLs
  for i in `find system -name '*.xml'`
  do
    xmlstarlet ed -d '//comment()' $i > /tmp/foo.xml
    cp /tmp/foo.xml $i
  done
  # remove whitespace from XMLs
  for i in `find system -name '*.xml'`
  do
    cat $i | sed -e '/^[ \t]*$/d;s,^[ \t]*,,;s,>[ \t]*<,><,g;s,[ \t]*$,,' > /tmp/foo.xml
    cp /tmp/foo.xml $i
  done
  # validate XMLs
  echo "Validating XML files in system"
  find system -name '*.xml' -exec xmlstarlet val -e {} \; | grep invalid


#============================================================================

  #echo "Copying noxxx system files"
  #cp -r ~/GoogleWiz/additions/noxsystem/* system


#============================================================================

  # remove empty directories (twice)
  echo "Removing empty directories"
  find . -type d -empty -exec rm -rf {} \; 2>/dev/null
  find . -type d -empty -exec rm -rf {} \; 2>/dev/null

}
COMMENT
